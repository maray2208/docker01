   #!/bin/bash
   sudo rm -rf /home/project/

   sudo mkdir -p /home/project/prometheus_stack/alertmanager
   sudo cp project/prometheus_stack/alertmanager/alertmanager.yml /home/project/prometheus_stack/alertmanager

   sudo mkdir -p /home/project/prometheus_stack/grafana/dashboards
   sudo cp project/prometheus_stack/grafana/dashboards/dashboards.yml /home/project/prometheus_stack/grafana/dashboards
   sudo cp project/prometheus_stack/grafana/dashboards/node-exporter-full_rev30.json /home/project/prometheus_stack/grafana/dashboards

   sudo mkdir -p /home/project/prometheus_stack/grafana/datasources
   sudo cp project/prometheus_stack/grafana/datasources/prometheus.yml /home/project/prometheus_stack/grafana/datasources

   sudo mkdir -p /home/project/prometheus_stack/prometheus
   sudo cp project/prometheus_stack/prometheus/prometheus.yml /home/project/prometheus_stack/prometheus
   sudo cp project/prometheus_stack/prometheus/alerts.yml /home/project/prometheus_stack/prometheus

   sudo mkdir -p /home/project/src/conf.d
   sudo cp project/src/conf.d/default.conf /home/project/src/conf.d
   
   sudo mkdir -p /home/project/src/html
   sudo cp project/src/html/index.php /home/project/src/html

   sudo cp project/docker-compose.yml /home/project
