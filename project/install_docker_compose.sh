#!/bin/bash
apt-get install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --batch --keyserver --dearmor --with-fingerprint -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"| tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update -y
apt-cache policy docker-ce
apt-get install docker-ce -y
systemctl status docker
mkdir -p ~/.docker/cli-plugins/
curl -SL https://github.com/docker/compose/releases/download/v2.14.2/docker-compose-linux-x86_64 -o ~/.docker/cli-plugins/docker-compose
chmod +x ~/.docker/cli-plugins/docker-compose

docker compose -f '/home/project/docker-compose.yml' up -d
cp /home/project/src/htmlphp/index.php /home/project/src/html/
ls /home/project/src/html/
